/* eslint-disable default-case */
import './custom.css';

import 'jquery';
import 'popper.js/dist/umd/popper';
import 'bootstrap/js/dist/button';
import 'bootstrap/js/dist/dropdown';
import 'bootstrap-material-design';
import 'bootstrap-material-design/dist/css/bootstrap-material-design.min.css';
import '@fortawesome/fontawesome-free/css/all.min.css';
import '@fortawesome/fontawesome-free/js/all';
import Slideout from 'slideout';

const POST = 'POST';
const GET = 'GET';
const LOADING = 'loading';
const ERROR = 'error';
const EMPTY = '<i class="fa fa-times" data-toggle="tooltip" title="Нет данных"></i>';
const MANUALY_DISABLED = '<i class="fa fa-ban" data-toggle="tooltip" title="Принудительно выключен"></i>';
const MANUALY_ENABLED = '<i class="fa fa-play" data-toggle="tooltip" title="Принудительно включен"></i>';
const AUTO_ON = '<i class="fa fa-check-circle" data-toggle="tooltip" title="Авто - Включен"></i>';
const AUTO_OFF = '<i class="fa fa-stop" data-toggle="tooltip" title="Авто - Выключен"></i>';
const TASK_HANDLER = '<span class="drag-handle"><i class="fa fa-sort" aria-hidden="true"></i></span>';

const portString = ':314';
let urlString = '';
let adress = '';
let rowIndexTaskTable = 1;
const dow = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];
const dowShrt = ['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ'];
const month = ['xxx', 'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
const phCalibrStatus = ['НЕТ', 'ДА'];
let clickIndexRow = 0;

const taskTableHdr = ['', 'Задача', 'Функция', 'Выход', 'Вкл', 'Выкл', 'Ярк.Вкл,%', 'Ярк.Выкл,%', 'Вход', 'Порог'];
const taskTableTxt = [
  [TASK_HANDLER, '1', 'ТО', 'Р1', '17:30', '20:40', '', '', '', ''],
];

function modalShow(id) {
  $(`#${id}`).show();
}
function modalShowWithDetails(id, messageId, message) {
  $(`#${id}`).modal({
    backdrop: 'static',
  });
  $(`#${messageId}`).text(message);
}
function modalHide(id) {
  $(`#${id}`).hide();
}

function sendRequest(type, url, data, callback) {
  modalShow(LOADING);
  $.ajax({
    url,
    type,
    tryCount: 0,
    retryLimit: 3,
    contentType: 'text/plain;charset=UTF-8',
    data,
    dataType: 'text',
    processData: false,
    timeout: 10000,
    success(response) {
      if (response && callback) { callback($.parseJSON(response.replace('--', '""'))); }
      modalHide(LOADING);
    },
    error(xhr, textStatus, errorThrown) {
      modalHide(LOADING);
      if (textStatus === 'timeout') {
        this.tryCount += 1;
        if (this.tryCount <= this.retryLimit) {
          // try again
          $.ajax(this);
          return;
        }
        modalShowWithDetails(ERROR, 'errorDetails',
          'Ответ не получен на протяжении 10 секунд. Проверьте адрес. Проверьте WiFi модуль контроллера.');
      } else if (xhr.status === 500) {
        modalShowWithDetails(ERROR, 'errorDetails',
          'Ошибка контроллера. Перезагрузите контроллер либо обратитесь в техподдержку.');
      } else {
        modalShowWithDetails(ERROR, 'errorDetails', errorThrown);
      }
    },
  });
}

function httpPostRequest(messageToSend, messageName, isBinary) {
  let data;
  if (isBinary) {
    data = messageToSend;
  } else {
    data = JSON.stringify(messageToSend);
  }
  const adr = `${adress}/${messageName}/`;

  sendRequest(POST, adr, data);
}

function httpGetRequest(messageName, callback) {
  const adr = `${adress}/${messageName}/`;
  sendRequest(GET, adr, null, callback);
}
//* ***********************************************************************************************

// EDIT KNOBS TABLE********************************************************************************
function setKnobsTable() {
  const cellData = [];
  const table = document.getElementById('knobsEditTable');

  for (let i = 0; i < table.rows[0].cells.length; i++) {
    cellData.push();
  }

  for (let i = 0; i < table.rows[0].cells.length; i++) {
    const cell = parseInt(table.rows[1].cells[i].childNodes[1].childNodes[0].value);
    if (isNaN(cell) || (cell !== 0 && cell !== 1 && cell !== 2)) {
      alert(`Статус кнопки номер ${i} введен неверно.`);
      return;
    }
    cellData[i] = cell;
  }

  httpPostRequest(cellData, 'stkb');
}

function getKnobsTable(cellDataR) {
  const table = document.getElementById('knobsEditTable');

  for (let i = 0; i < table.rows[0].cells.length; i++) {
    const select = table.rows[1].cells[i].childNodes[1].childNodes[0];
    select.value = cellDataR[i];
    select.disabled = false;
    changeColor(select);
  }

  document.getElementById('btnPostKnobsValues').disabled = false;
}

//* ***********************************************************************************************

// EDIT FAN TABLE***********************************************************************************
function setFanTable() {
  const cellData = [,];

  const input = document.getElementById('inFanPower');
  const select = document.getElementById('sltFanMod');

  const power = parseInt(input.value);
  if (isNaN(power) || power < 0 || power > 99) {
    alert('Мощность введена неверно. (0-99)');
    return;
  }
  cellData[0] = parseInt(power);


  const mode = parseInt(select.value);
  if (isNaN(mode) || (mode !== 0 && mode !== 1)) {
    alert('Статус введен неверно.');
    return;
  }
  cellData[1] = mode;

  httpPostRequest(cellData, 'stfn');
}

function getFanTable(cellDataR) {
  const input = document.getElementById('inFanPower');
  const select = document.getElementById('sltFanMod');

  input.value = null;
  if (!isNaN(cellDataR[0])) {
    input.value = cellDataR[0];
  }

  select.value = cellDataR[1];
  if (cellDataR[1] === 0) {
    select.style.color = 'red';
  } else if (cellDataR[1] === 1) {
    select.style.color = 'green';
  }

  select.disabled = false;
  document.getElementById('btnPostFanValues').disabled = false;
}

//* ************************************************************************************************

// SEARCH TERMOSENSORS******************************************************************************
function showSensorSearchResults(data) {
  const table = document.getElementById('termosensTable');
  if (data[0] !== -99.9) {
    table.rows[1].cells[0].innerHTML = data[0];
  } else {
    table.rows[1].cells[0].innerHTML = EMPTY;
  }

  if (data[1] !== -99.9) {
    table.rows[1].cells[1].innerHTML = data[1];
  } else {
    table.rows[1].cells[1].innerHTML = EMPTY;
  }

  $('#btnStopSearchTSensor').disabled = false;
}

function searchTSReq(task) {
  if (task === 'tsrc') {
    httpGetRequest(task, showSensorSearchResults);
    document.getElementById('btnStopSearchTSensor').disabled = false;
  } else {
    httpGetRequest(task);
    document.getElementById('btnStopSearchTSensor').disabled = true;
  }
}

// TEST LEDS***************************************************************
function testLedsReq(task) {
  const adr = `${adress}/${task}/`;

  switch (task) {
    case 'gttb':
      document.getElementById('btnEndLedTest').disabled = false;
      document.getElementById('btnStartLedTest').disabled = true;
      break;
    case 'gtte':
      document.getElementById('btnEndLedTest').disabled = true;
      document.getElementById('btnStartLedTest').disabled = false;
      break;
  }

  httpGetRequest(adr);
}
//* ***********************************************************************

// CALIBR PH***************************************************************
function calibrPhReq(task) {
  const table = document.getElementById('calibrPhTable');

  let obj = [];
  for (let i = 0; i < 4; i++) {
    obj.push('\xa0');
  }

  switch (task) {
    case 'cphb':
      document.getElementById('btnReadyPh4').disabled = false;
      break;
    case 'cph4':
      document.getElementById('btnReadyPh4').disabled = true;
      document.getElementById('btnReadyPh7').disabled = false;
      break;
    case 'cph7':
      document.getElementById('btnReadyPh7').disabled = true;
      break;
    case 'cphe':
      break;
  }

  if (task === 'cph7') {
    httpGetRequest(task, () => {
      obj = JSON.parse(xmlhttp.responseText);
      table.rows[1].cells[0].innerHTML = obj[0];
      table.rows[1].cells[1].innerHTML = obj[1];
      table.rows[1].cells[2].innerHTML = obj[2];
      table.rows[1].cells[3].innerHTML = phCalibrStatus[obj[3]];
    });
  } else {
    httpGetRequest(task);
  }
}
//* *******************************************************************************************

// EDIT MOON LIGHT******************************************************************************
function setMoonLightTable() {
  const cellData = ['', '', ''];
  const reqData = [, , , ,];
  const pattern = new RegExp(/([01]\d|2[0-3]):?[0-5]\d/);

  const startHour = document.getElementById('inMoonStartHour').value;
  const endHour = document.getElementById('inMoonEndHour').value;
  const mode = parseInt(document.getElementById('inMoonMode').value);


  if (pattern.exec(startHour) == null) {
    alert('Время ВКЛ введено неверно. (ЧЧ:ММ)');
    return;
  }
  cellData[0] = startHour;

  if (pattern.exec(endHour) == null) {
    alert('Время ВЫКЛ введено неверно. (ЧЧ:ММ)');
    return;
  }
  cellData[1] = endHour;

  if (isNaN(mode) || mode < 0 || mode > 1) {
    alert('Статус введен неверно.');
    return;
  }

  reqData[0] = parseInt(cellData[0].substr(0, 2));
  reqData[1] = parseInt(cellData[0].substr(3, 2));
  reqData[2] = parseInt(cellData[1].substr(0, 2));
  reqData[3] = parseInt(cellData[1].substr(3, 2));
  reqData[4] = mode;

  httpPostRequest(reqData, 'stml');
}

function getMoonLightTable(cellDataR) {
  const cellData = ['', '', ''];

  if (!isNaN(cellDataR[0])) {
    if (cellDataR[0] < 9) {
      cellData[0] = `0${cellDataR[0]}:`;
    } else {
      cellData[0] = `${cellDataR[0]}:`;
    }
    if (cellDataR[1] < 9) {
      cellData[0] += `0${cellDataR[1]}`;
    } else {
      cellData[0] += cellDataR[1];
    }

    if (cellDataR[2] < 9) {
      cellData[1] = `0${cellDataR[2]}:`;
    } else {
      cellData[1] = `${cellDataR[2]}:`;
    }
    if (cellDataR[3] < 9) {
      cellData[1] += `0${cellDataR[3]}`;
    } else {
      cellData[1] += cellDataR[3];
    }
  }

  const startHour = document.getElementById('inMoonStartHour');
  const endHour = document.getElementById('inMoonEndHour');
  const mode = document.getElementById('inMoonMode');
  const saveButton = document.getElementById('bntPostMoonValues');

  startHour.value = cellData[0];
  endHour.value = cellData[1];
  mode.value = cellDataR[4];
  changeColor(mode);

  startHour.disabled = false;
  endHour.disabled = false;
  mode.disabled = false;
  saveButton.disabled = false;
}
//* ********************************************************************************************
function timeSeing() {
  const dat = new Date();
  document.getElementById('dat1').innerHTML = dow[dat.getDay()];
  document.getElementById('dat2').innerHTML = dat.getHours();
  document.getElementById('dat3').innerHTML = dat.getMinutes();
  document.getElementById('dat4').innerHTML = dat.getDate();
  document.getElementById('dat5').innerHTML = dat.getMonth() + 1;
  document.getElementById('dat6').innerHTML = dat.getFullYear();
}

// EDIT SYSTEM DATE*****************************************************************************
const timeInterVar = setInterval(() => {
  timeSeing();
}, 1000);

function setDate() {
  const cellData = [];
  const dat = new Date();
  cellData.push(dat.getDay());
  cellData.push(dat.getHours());
  cellData.push(dat.getMinutes());
  cellData.push(dat.getDate());
  cellData.push(dat.getMonth() + 1);
  cellData.push(dat.getFullYear() - 2000);
  httpPostRequest(cellData, 'stdt');
}
//* ****************************************************************************

// SHOW DISPLAY*****************************************************************
function getDisplay(cellDataR) {
  const table2 = document.getElementById('displTable2');
  const table3 = document.getElementById('displTable3');

  for (var i = 0; i < 12; i++) {
    const tableCell = document.getElementById(`stVal${i}`);
    switch (i) {
      case 0:
      case 1:
      case 2:
        if (cellDataR[i] === -99.9) {
          cellDataR[i] = EMPTY;
        }
        tableCell.innerHTML = cellDataR[i]; // 0...11
        tableCell.style.color = 'black';
        break;
      default:
        if (cellDataR[i] === 0) {
          tableCell.innerHTML = MANUALY_DISABLED;
          tableCell.style.color = 'red';
        } else if (cellDataR[i] === 2) {
          tableCell.innerHTML = AUTO_ON;
          tableCell.style.color = 'green';
        } else if (cellDataR[i] === 1) {
          tableCell.innerHTML = AUTO_OFF;
          tableCell.style.color = 'red';
        } else if (cellDataR[i] === 3) {
          tableCell.innerHTML = MANUALY_ENABLED;
          tableCell.style.color = 'blue';
        } else {
          tableCell.innerHTML = EMPTY;
          tableCell.style.color = 'black';
        }
        break;
    }
  }
  for (var i = 0; i < 6; i++) {
    table2.rows[1].cells[i].innerHTML = `${cellDataR[i + 12]}%`; // 0...5
  }
  for (var i = 0; i < 6; i++) {
    if (i === 0) {
      table3.rows[1].cells[i].innerHTML = dow[cellDataR[i + 18]];
    } else if (i === 1) {
      table3.rows[1].cells[i].innerHTML = digitToTimeView(cellDataR[i + 18]);
    } else if (i === 2) {
      table3.rows[1].cells[i].innerHTML = digitToTimeView(cellDataR[i + 18]);
    } else if (i === 4) {
      table3.rows[1].cells[i].innerHTML = month[cellDataR[i + 18]];
    } else if (i === 5) {
      table3.rows[1].cells[i].innerHTML = cellDataR[i + 18] + 2000;
    } else {
      table3.rows[1].cells[i].innerHTML = cellDataR[i + 18]; // 0...5
    }
  }
}

let timerXhrTask;

function xhrTimer(item) {
  const chk = document.getElementById('GetDisplCheck').checked;
  if (chk === true) {
    if (item.id === 'd_1') {
      timerXhrTask = setInterval(() => {
        httpGetRequest('gtdl', getDisplay);
      }, 30000);
    } else {
      clearInterval(timerXhrTask);
    }
  }
}

function getDisplTimer() {
  const chk = document.getElementById('GetDisplCheck').checked;

  if (chk === true) {
    document.getElementById('btnDispl').disabled = true;
    timerXhrTask = setInterval(() => {
      httpGetRequest('gtdl', getDisplay);
    }, 30000);
  } else {
    document.getElementById('btnDispl').disabled = false;
    clearInterval(timerXhrTask);
  }
}

//* ****************************************************************************


// EDIT TASK'S TABLE************************************************************
function _enum(list) {
  for (const key in list) {
    list[list[key] = list[key]] = key;
  }
  return Object.freeze(list);
}

const DATAOFFSET = _enum({
  numberLine: 0,
  functionNumber: 1,
  inputNumber: 2,
  timeOnSec: 3,
  timeOnMin: 4,
  timeOnHrs: 5,
  timeOnDow: 6,
  timeOffSec: 7,
  timeOffMin: 8,
  timeOffHrs: 9,
  timeOffDow: 10,
  pwmBegin: 11,
  pwmEnd: 12,
  temperature: 13,
});
const NUMBERLINE = _enum({
  P1: 0,
  P2: 1,
  P3: 2,
  P4: 3,
  P5: 4,
  P6: 5,
  P7: 6,
  P8: 7,
  C1: 9,
  C2: 10,
  C3: 11,
  C4: 12,
  C5: 13,
  C6: 14,
  BP: 15,
});
const FUNCTION = _enum({
  TC: 0,
  TM: 1,
  TO: 2,
  HT: 3,
  OT: 4,
  TH: 5,

  CO: 7,
  TK: 8,
  UV: 9,
});
const TASKTABLE = _enum({
  HANDLER: 0,
  NUMBER: 1,
  FUNCTION: 2,
  OUT: 3,
  TIMEON: 4,
  TIMEOFF: 5,
  PWMON: 6,
  PWMOFF: 7,
  INPUT: 8,
  TRESH: 9,
});
const INPUT = _enum({
  T1: 0,
  T2: 1,
  X: 2,
  OPEN: 3,
  SHRT: 4,
  PH: 5,
});

// CREATE TASK TABLE************************************************************
function populateTable(table, rows, cells, content) {
  const is_func = (typeof content === 'function');
  if (!table) { table = document.createElement('table'); }
  table.className = 'table table-bordered table-striped table-hover';
  table.id = 'taskTable';

  const header = table.createTHead();
  const rowh = header.insertRow(0);
  for (let i = 0; i < cells; ++i) {
    const cell = rowh.insertCell(i);
    cell.outerHTML = `<th>${taskTableHdr[i]}</th>`;
  }

  const body = table.createTBody();
  body.setAttribute('id', 'taskRows');
  for (let i = 0; i < rows; ++i) {
    const row = body.insertRow(i);
    row.setAttribute('class', 'taskRow');
    row.onclick = function () {
      rowIndexTaskTable = this.rowIndex;

      if (clickIndexRow !== 0) {
        if (clickIndexRow % 2 !== 0) {
          table.rows[clickIndexRow].style.backgroundColor = '#fff';
          table.rows[clickIndexRow].style.color = 'black';
        } else {
          table.rows[clickIndexRow].style.backgroundColor = '#d0d0d0';
          table.rows[clickIndexRow].style.color = 'black';
        }
      }

      this.style.backgroundColor = 'black';
      this.style.color = 'white';
      clickIndexRow = this.rowIndex;

      const tableEdit = document.getElementById('taskEditTable');
      for (let i = 1; i < cells; i++) {
        tableEdit.rows[1].cells[i].childNodes[1].childNodes[0].value = this.cells[i].innerHTML;
      }
    };

    for (let j = 0; j < cells; ++j) {
      const cell = row.insertCell(j);
      const text = content[i][j];
      cell.innerHTML = text;
    }
  }
  return table;
}
// ADD ROW TO TASKTABLE********************************************************
function addRow(tableID, rowNumber, content) {
  const table = document.getElementById(tableID);

  const rowCount = table.rows.length;
  const cellCount = table.rows[0].cells.length;

  if (rowCount >= 100) {
    return;
  }
  const row = table.insertRow(rowCount);
  row.onclick = function () {
    rowIndexTaskTable = this.rowIndex;

    if (clickIndexRow !== 0) {
      if (clickIndexRow % 2 !== 0) {
        table.rows[clickIndexRow].style.backgroundColor = '#fff';
        table.rows[clickIndexRow].style.color = 'black';
      } else {
        table.rows[clickIndexRow].style.backgroundColor = '#d0d0d0';
        table.rows[clickIndexRow].style.color = 'black';
      }
    }

    this.style.backgroundColor = 'black';
    this.style.color = 'white';
    clickIndexRow = this.rowIndex;

    const tableEdit = document.getElementById('taskEditTable');
    for (let i = 1; i < cellCount; i++) {
      tableEdit.rows[1].cells[i].childNodes[1].childNodes[0].value = this.cells[i].innerHTML;
    }
  };

  for (let j = 0; j < cellCount; j++) {
    const cellTd = row.insertCell(j);
    const tableEdit = document.getElementById('taskEditTable');

    if (j === 0) {
      cellTd.innerHTML = TASK_HANDLER;
    } else if (j === 1) {
      cellTd.innerHTML = rowCount; // 0 - headline
      tableEdit.rows[1].cells[1].childNodes[1].childNodes[0].value = rowCount;
    } else if (rowNumber !== undefined) {
      cellTd.innerHTML = table.rows[rowNumber].cells[j].innerHTML;
      tableEdit.rows[1].cells[j].childNodes[1].childNodes[0].value = table.rows[rowNumber].cells[j].innerHTML;
    } else {
      cellTd.innerHTML = table.rows[rowCount - 1].cells[j].innerHTML;
    }
  }
  rowIndexTaskTable = rowCount;
}

// DELETE ROW FROM TASKTABLE********************************************************
function delRow(tableId, row) {
  const table = document.getElementById(tableId);
  const tableEdit = document.getElementById('taskEditTable');
  const cellCount = table.rows[0].cells.length;

  if ((table.rows.length > 2)) { // 1- headline 2- first task
    table.deleteRow(row);
    for (var i = 1; i < table.rows.length; i++) {
      table.rows[i].cells[0].innerHTML = i;
      if ((i === clickIndexRow) && (clickIndexRow !== rowIndexTaskTable)) {
        continue;
      }
      if (i % 2 !== 0) {
        table.rows[i].style.backgroundColor = '#fff';
        table.rows[i].style.color = 'black';
      } else {
        table.rows[i].style.backgroundColor = '#d0d0d0';
        table.rows[i].style.color = 'black';
      }
    }

    if (rowIndexTaskTable >= table.rows.length) {
      rowIndexTaskTable = row -= 1;
    }

    if (clickIndexRow >= table.rows.length) {
      clickIndexRow = 0;
    }

    for (let i = 1; i < cellCount; i++) {
      tableEdit.rows[1].cells[i].childNodes[1].childNodes[0].value = table.rows[row].cells[i].innerHTML;
    }
  }
}

// CREATE EDITOR TABLE************************************************************
function populateEditorTable(table, rows, cells, content) {
  const is_func = (typeof content === 'function');
  if (!table) { table = document.createElement('table'); }
  table.className = 'scrTab';
  table.id = 'taskEditTable';
  const rowh = document.createElement('tr');
  for (let i = 0; i < cells; i++) {
    rowh.appendChild(document.createElement('th'));
    rowh.cells[i].appendChild(document.createTextNode(taskTableHdr[i]));
  }

  table.appendChild(rowh);
  for (let i = 0; i < rows; i++) {
    const row = document.createElement('tr');
    for (let j = 0; j < cells; ++j) {
      const elInput = document.createElement('input');
      elInput.setAttribute('type', 'text');
      elInput.className = 'inEditTbl';
      if (j === 0) {
        elInput.setAttribute('readonly', 'readonly');
      }
      const elTd = document.createElement('td');
      elTd.appendChild(elInput);
      row.appendChild(elTd);
    }
    table.appendChild(row);
  }
  return table;
}

// EDIT ROW TO TASKTABLE********************************************************
function editRow(tableID, cells) {
  const table = document.getElementById(tableID);
  const tableEdit = document.getElementById('taskEditTable');

  if (tableEdit.rows[1].cells[TASKTABLE.NUMBER].childNodes[1].value == '') { // empty input
    alert('Ошибка: Невыбрана Задача.');
    return;
  }
  switch (tableEdit.rows[1].cells[TASKTABLE.FUNCTION].childNodes[1].childNodes[0].value) {
    case 'ТО':
    case 'ТС':
    case 'ТМ':
    case 'НТ':
    case 'ОТ':
    case 'ТН':
    case 'СО':
    case 'ЦВ':
    case 'УВ':
      break;
    default:
      alert('Ошибка: Неверно введена Функция');
      return;
  }
  switch (tableEdit.rows[1].cells[TASKTABLE.OUT].childNodes[1].childNodes[0].value) {
    case 'Р1':
    case 'Р2':
    case 'Р3':
    case 'Р4':
    case 'Р5':
    case 'Р6':
    case 'Р7':
    case 'Р8':
    case 'С1':
    case 'С2':
    case 'С3':
    case 'С4':
    case 'С5':
    case 'С6':
    case 'ВР':
      break;
    default:
      alert('Ошибка: Неверно введен Выход');
      return;
  }

  switch (tableEdit.rows[1].cells[TASKTABLE.OUT].childNodes[1].childNodes[0].value) {
    case 'Р1':
    case 'Р2':
    case 'Р3':
    case 'Р4':
    case 'Р5':
    case 'Р6':
    case 'Р7':
    case 'Р8':
    case 'ВР':
      tableEdit.rows[1].cells[TASKTABLE.PWMON].childNodes[1].childNodes[0].value = '';
      tableEdit.rows[1].cells[TASKTABLE.PWMOFF].childNodes[1].childNodes[0].value = '';
      break;

    case 'С1':
    case 'С2':
    case 'С3':
    case 'С4':
    case 'С5':
    case 'С6':
      switch (tableEdit.rows[1].cells[TASKTABLE.FUNCTION].childNodes[1].childNodes[0].value) {
        case 'УВ':
        case 'НТ':
        case 'ОТ':
        case 'ЦВ':
          alert('Ошибка: Функция не соответствует Выходу');
          return;
          break;
      }
      var pwmS = parseInt(tableEdit.rows[1].cells[TASKTABLE.PWMON].childNodes[1].childNodes[0].value);
      if (pwmS < 0 || pwmS > 99 || isNaN(pwmS)) {
        alert('Ошибка: Ярк.Вкл вне диапазона');
        return;
      }
      var pwmE = parseInt(tableEdit.rows[1].cells[TASKTABLE.PWMON].childNodes[1].childNodes[0].value);
      if (pwmE < 0 || pwmE > 99 || isNaN(pwmE)) {
        alert('Ошибка: Ярк.Выкл вне диапазона');
        return;
      }
      tableEdit.rows[1].cells[TASKTABLE.INPUT].childNodes[1].childNodes[0].value = '';
      tableEdit.rows[1].cells[TASKTABLE.TRESH].childNodes[1].childNodes[0].value = '';
      break;
  }

  switch (tableEdit.rows[1].cells[TASKTABLE.FUNCTION].childNodes[1].childNodes[0].value) {
    case 'ТС':
      {
        var date = tableEdit.rows[1].cells[TASKTABLE.TIMEON].childNodes[1].childNodes[0].value;
        var pattern = /^([0-1][0-9]|2[0-3]):[0-5][0-9]$/;
        if (!pattern.test(date)) {
          alert('Дата Вкл введена неверно. (ЧЧ:ММ)');
          return;
        }
        date = tableEdit.rows[1].cells[TASKTABLE.TIMEOFF].childNodes[1].childNodes[0].value;
        pattern = /^[0-9][0-9]$/;
        if (!pattern.test(date)) {
          alert('Дата Выкл введена неверно. (СС)');
          return;
        }
        tableEdit.rows[1].cells[TASKTABLE.INPUT].childNodes[1].childNodes[0].value = '';
        tableEdit.rows[1].cells[TASKTABLE.TRESH].childNodes[1].childNodes[0].value = '';
      }
      break;
    case 'ТМ':
      {
        var date = tableEdit.rows[1].cells[TASKTABLE.TIMEON].childNodes[1].childNodes[0].value;
        var pattern = /^([0-1][0-9]|2[0-3]):[0-5][0-9]$/;
        if (!pattern.test(date)) {
          alert('Дата Вкл введена неверно. (ЧЧ:ММ)');
          return;
        }
        date = tableEdit.rows[1].cells[TASKTABLE.TIMEOFF].childNodes[1].childNodes[0].value;
        pattern = /^[0-9][0-9]$/;
        if (!pattern.test(date)) {
          alert('Дата Выкл введена неверно. (MM)');
          return;
        }
        tableEdit.rows[1].cells[TASKTABLE.INPUT].childNodes[1].childNodes[0].value = '';
        tableEdit.rows[1].cells[TASKTABLE.TRESH].childNodes[1].childNodes[0].value = '';
      }
      break;
    case 'ТО':
      var date = tableEdit.rows[1].cells[TASKTABLE.TIMEON].childNodes[1].childNodes[0].value;
      var pattern = /^([0-1][0-9]|2[0-3]):[0-5][0-9]$/;
      if (!pattern.test(date)) {
        alert('Дата Вкл введена неверно. (ЧЧ:ММ)');
        return;
      }
      date = tableEdit.rows[1].cells[TASKTABLE.TIMEOFF].childNodes[1].childNodes[0].value;
      pattern = /^([0-1][0-9]|2[0-3]):[0-5][0-9]$/;
      if (!pattern.test(date)) {
        alert('Дата Выкл введена неверно. (ЧЧ:MM)');
        return;
      }
      // tableEdit.rows[1].cells[TASKTABLE.INPUT].childNodes[0].value = '';
      tableEdit.rows[1].cells[TASKTABLE.INPUT].childNodes[1].childNodes[0].value = '';
      tableEdit.rows[1].cells[TASKTABLE.TRESH].childNodes[1].childNodes[0].value = '';
      // console.log(tableEdit.rows[1].cells[TASKTABLE.INPUT].childNodes);
      break;
    case 'ТН':
      {
        var date = tableEdit.rows[1].cells[TASKTABLE.TIMEON].childNodes[1].childNodes[0].value;
        var pattern = /^(ПН |ВТ |СР |ЧТ |ПТ |СБ |ВС )?([0-1][0-9]|2[0-3]):[0-5][0-9]$/;
        if (!pattern.test(date)) {
          alert('Дата Вкл введена неверно. (ДН ЧЧ:ММ)');
          return;
        }
        date = tableEdit.rows[1].cells[TASKTABLE.TIMEOFF].childNodes[1].childNodes[0].value;
        pattern = /^[0-9][0-9]$/;
        if (!pattern.test(date)) {
          alert('Дата Выкл введена неверно. (СС)');
          return;
        }
        tableEdit.rows[1].cells[TASKTABLE.INPUT].childNodes[1].childNodes[0].value = '';
        tableEdit.rows[1].cells[TASKTABLE.TRESH].childNodes[1].childNodes[0].value = '';
      }
      break;
    case 'ЦВ':
      {
        var date = tableEdit.rows[1].cells[TASKTABLE.TIMEON].childNodes[1].childNodes[0].value;
        var pattern = /^[0-9][0-9]$/;

        if (!pattern.test(date)) {
          alert('Дата Вкл введена неверно. (СС 00-99)');
          return;
        }
        const dateOn = parseInt(date);
        date = tableEdit.rows[1].cells[TASKTABLE.TIMEOFF].childNodes[1].childNodes[0].value;
        pattern = /^[0-9][0-9]$/;
        if (!pattern.test(date)) {
          alert('Дата Выкл введена неверно. (СС 00-99)');
          return;
        }
        const dateOff = parseInt(date);
        if (dateOff >= dateOn) {
          alert('Период Вкл должен быть больше времени Выкл. (СС 00-99)');
          return;
        }
        tableEdit.rows[1].cells[TASKTABLE.INPUT].childNodes[1].value = '';
        tableEdit.rows[1].cells[TASKTABLE.TRESH].childNodes[1].value = '';
      }
      break;
    case 'НТ':
    case 'ОТ':
      {
        var tresh = parseFloat(tableEdit.rows[1].cells[TASKTABLE.TRESH].childNodes[1].value);
        if (tresh < 0 || tresh > 99 || isNaN(tresh)) {
          alert('Ошибка: Порог задан вне диапазона.(0-99)');
          return;
        }
        switch (tableEdit.rows[1].cells[TASKTABLE.INPUT].childNodes[1].value) {
          case 't1':
          case 't2':
          case 'X':
            break;
          default:
            alert('Ошибка: Неверно введен Вход. (Т1,Т2)');
            return;
        }
        tableEdit.rows[1].cells[TASKTABLE.PWMON].childNodes[1].value = '';
        tableEdit.rows[1].cells[TASKTABLE.PWMOFF].childNodes[1].value = '';
        tableEdit.rows[1].cells[TASKTABLE.TIMEON].childNodes[1].value = '';
        tableEdit.rows[1].cells[TASKTABLE.TIMEOFF].childNodes[1].value = '';
      }
      break;
    case 'СО':
      {
        var tresh = parseFloat(tableEdit.rows[1].cells[TASKTABLE.TRESH].childNodes[1].value);
        if (tresh < 0 || tresh > 9.9 || isNaN(tresh)) {
          alert('Ошибка: Порог задан вне диапазона.(0-9.9)');
          return;
        }
        tableEdit.rows[1].cells[TASKTABLE.PWMON].childNodes[1].value = '';
        tableEdit.rows[1].cells[TASKTABLE.PWMOFF].childNodes[1].value = '';
        tableEdit.rows[1].cells[TASKTABLE.TIMEON].childNodes[1].value = '';
        tableEdit.rows[1].cells[TASKTABLE.TIMEOFF].childNodes[1].value = '';
        tableEdit.rows[1].cells[TASKTABLE.INPUT].childNodes[1].value = '';
      }
      break;
    case 'УВ':
      var date = tableEdit.rows[1].cells[TASKTABLE.TIMEOFF].childNodes[1].childNodes[0].value;
      var pattern = /^([0-1][0-9]|2[0-3]):[0-5][0-9]$/;
      if (!pattern.test(date)) {
        alert('Дата Выкл введена неверно. (ЧЧ:ММ)');
        return;
      }
      switch (tableEdit.rows[1].cells[TASKTABLE.INPUT].childNodes[1].value) {
        case 'З':
        case 'Р':
          break;
        default:
          alert('Ошибка: Неверно введен Вход. (З,Р)');
          return;
      }
      tableEdit.rows[1].cells[TASKTABLE.TIMEON].childNodes[1].value = '';
      tableEdit.rows[1].cells[TASKTABLE.PWMON].childNodes[1].value = '';
      tableEdit.rows[1].cells[TASKTABLE.PWMOFF].childNodes[1].value = '';
      tableEdit.rows[1].cells[TASKTABLE.TRESH].childNodes[1].value = '';

      break;
  }

  for (let j = 1; j < cells; j++) {
    table.rows[rowIndexTaskTable].cells[j].innerHTML = tableEdit.rows[1].cells[j].childNodes[1].childNodes[0].value;
    // content[rowIndexTaskTable - 1][j] = tableEdit.rows[1].cells[j].childNodes[1].childNodes[0].value;
  }

  // console.log(taskTableTxt);
}

function setTaskTable() {
  const taskTable = document.getElementById('taskTable');
  const tableRowsCount = taskTable.rows.length - 1; // - table's header
  const deviceTaskStructByteCount = 15;
  const bufferTaskTable = new ArrayBuffer(deviceTaskStructByteCount * tableRowsCount);

  // names from device's task c struct
  for (let i = 0; i < tableRowsCount; i++) {
    const dataView = new DataView(bufferTaskTable, i * deviceTaskStructByteCount, deviceTaskStructByteCount);
    const cellOut = taskTable.rows[i + 1].cells[TASKTABLE.OUT].innerHTML; // .childNodes[0].value;
    const cellFunction = taskTable.rows[i + 1].cells[TASKTABLE.FUNCTION].innerHTML; // childNodes[0].value;
    const cellTimeOn = taskTable.rows[i + 1].cells[TASKTABLE.TIMEON].innerHTML; // childNodes[0].value;
    const cellTimeOff = taskTable.rows[i + 1].cells[TASKTABLE.TIMEOFF].innerHTML; // childNodes[0].value;
    const cellPwmOn = taskTable.rows[i + 1].cells[TASKTABLE.PWMON].innerHTML; // childNodes[0].value;
    const cellPwmOff = taskTable.rows[i + 1].cells[TASKTABLE.PWMOFF].innerHTML; // childNodes[0].value;
    const cellInput = taskTable.rows[i + 1].cells[TASKTABLE.INPUT].innerHTML; // childNodes[0].value;
    const cellTresh = taskTable.rows[i + 1].cells[TASKTABLE.TRESH].innerHTML; // childNodes[0].value;

    switch (cellOut) { // numberline
      case 'Р1':
        dataView.setUint8(DATAOFFSET.numberLine, NUMBERLINE.P1);
        break;
      case 'Р2':
        dataView.setUint8(DATAOFFSET.numberLine, NUMBERLINE.P2);
        break;
      case 'Р3':
        dataView.setUint8(DATAOFFSET.numberLine, NUMBERLINE.P3);
        break;
      case 'Р4':
        dataView.setUint8(DATAOFFSET.numberLine, NUMBERLINE.P4);
        break;
      case 'Р5':
        dataView.setUint8(DATAOFFSET.numberLine, NUMBERLINE.P5);
        break;
      case 'Р6':
        dataView.setUint8(DATAOFFSET.numberLine, NUMBERLINE.P6);
        break;
      case 'Р7':
        dataView.setUint8(DATAOFFSET.numberLine, NUMBERLINE.P7);
        break;
      case 'Р8':
        dataView.setUint8(DATAOFFSET.numberLine, NUMBERLINE.P8);
        break;
      case 'С1':
        dataView.setUint8(DATAOFFSET.numberLine, NUMBERLINE.C1);
        break;
      case 'С2':
        dataView.setUint8(DATAOFFSET.numberLine, NUMBERLINE.C2);
        break;
      case 'С3':
        dataView.setUint8(DATAOFFSET.numberLine, NUMBERLINE.C3);
        break;
      case 'С4':
        dataView.setUint8(DATAOFFSET.numberLine, NUMBERLINE.C4);
        break;
      case 'С5':
        dataView.setUint8(DATAOFFSET.numberLine, NUMBERLINE.C5);
        break;
      case 'С6':
        dataView.setUint8(DATAOFFSET.numberLine, NUMBERLINE.C6);
        break;
      case 'ВР':
        dataView.setUint8(DATAOFFSET.numberLine, NUMBERLINE.BP);
        break;
    } // numberline

    switch (cellFunction) {
      case 'ТО':
        dataView.setUint8(DATAOFFSET.functionNumber, FUNCTION.TO);
        break;
      case 'ТС':
        dataView.setUint8(DATAOFFSET.functionNumber, FUNCTION.TC);
        break;
      case 'ТМ':
        dataView.setUint8(DATAOFFSET.functionNumber, FUNCTION.TM);
        break;
      case 'НТ':
        dataView.setUint8(DATAOFFSET.functionNumber, FUNCTION.HT);
        break;
      case 'ОТ':
        dataView.setUint8(DATAOFFSET.functionNumber, FUNCTION.OT);
        break;
      case 'ТН':
        dataView.setUint8(DATAOFFSET.functionNumber, FUNCTION.TH);
        break;
      case 'СО':
        dataView.setUint8(DATAOFFSET.functionNumber, FUNCTION.CO);
        break;
      case 'ЦВ':
        dataView.setUint8(DATAOFFSET.functionNumber, FUNCTION.TK);
        break;
      case 'УВ':
        dataView.setUint8(DATAOFFSET.functionNumber, FUNCTION.UV);
        break;
    } // functionNumber

    if (!(cellTimeOn === '')) {
      switch (cellFunction) {
        case 'ТО':
          {
            const hours = parseInt(cellTimeOn.substr(0, 2));
            const minutes = parseInt(cellTimeOn.substr(3, 2));
            dataView.setUint8(DATAOFFSET.timeOnHrs, hours);
            dataView.setUint8(DATAOFFSET.timeOnMin, minutes);
          }
          break;
        case 'ТС':
          {
            const hours = parseInt(cellTimeOn.substr(0, 2));
            const minutes = parseInt(cellTimeOn.substr(3, 2));
            dataView.setUint8(DATAOFFSET.timeOnHrs, hours);
            dataView.setUint8(DATAOFFSET.timeOnMin, minutes);
          }
          break;
        case 'ТМ':
          {
            const hours = parseInt(cellTimeOn.substr(0, 2));
            const minutes = parseInt(cellTimeOn.substr(3, 2));
            dataView.setUint8(DATAOFFSET.timeOnHrs, hours);
            dataView.setUint8(DATAOFFSET.timeOnMin, minutes);
          }
          break;
        case 'ТН':
          {
            var dow = cellTimeOn.substr(0, 2);
            switch (dow) {
              case 'ВС':
                dow = 0;
                break;
              case 'ПН':
                dow = 1;
                break;
              case 'ВТ':
                dow = 2;
                break;
              case 'СР':
                dow = 3;
                break;
              case 'ЧТ':
                dow = 4;
                break;
              case 'ПТ':
                dow = 5;
                break;
              case 'СБ':
                dow = 6;
                break;
            }
            var hours = parseInt(cellTimeOn.substr(3, 2));
            var minutes = parseInt(cellTimeOn.substr(6, 2));
            dataView.setUint8(DATAOFFSET.timeOnDow, dow);
            dataView.setUint8(DATAOFFSET.timeOnHrs, hours);
            dataView.setUint8(DATAOFFSET.timeOnMin, minutes);
          }
          break;
        case 'ЦВ':
          var seconds = parseInt(cellTimeOn);
          var minutes = parseInt(seconds / 60);
          seconds %= 60;
          dataView.setUint8(DATAOFFSET.timeOnMin, minutes);
          dataView.setUint8(DATAOFFSET.timeOnSec, seconds);
          break;
      }
    }
    if (!(cellTimeOff == '')) {
      switch (cellFunction) {
        case 'ТО':
          var hours = parseInt(cellTimeOff.substr(0, 2));
          var minutes = parseInt(cellTimeOff.substr(3, 2));
          dataView.setUint8(DATAOFFSET.timeOffHrs, hours);
          dataView.setUint8(DATAOFFSET.timeOffMin, minutes);
          break;
        case 'ТС':
          var seconds = parseInt(cellTimeOff);
          var hours = parseInt(cellTimeOn.substr(0, 2));
          var minutes = parseInt(cellTimeOn.substr(3, 2));
          seconds = seconds + minutes * 60 + hours * 3600;
          seconds %= (24 * 3600);
          hours = parseInt(seconds / 3600);
          seconds %= 3600;
          minutes = parseInt(seconds / 60);
          seconds %= 60;
          dataView.setUint8(DATAOFFSET.timeOffHrs, hours);
          dataView.setUint8(DATAOFFSET.timeOffMin, minutes);
          dataView.setUint8(DATAOFFSET.timeOffSec, seconds);
          break;
        case 'ТМ':
          var seconds = parseInt(cellTimeOff) * 60;
          var hours = parseInt(cellTimeOn.substr(0, 2));
          var minutes = parseInt(cellTimeOn.substr(3, 2));
          seconds = seconds + minutes * 60 + hours * 3600;
          seconds %= (24 * 3600);
          hours = parseInt(seconds / 3600);
          seconds %= 3600;
          minutes = parseInt(seconds / 60);
          // seconds=seconds%60;
          dataView.setUint8(DATAOFFSET.timeOffHrs, hours);
          dataView.setUint8(DATAOFFSET.timeOffMin, minutes);
          // dataView.setUint8(DATAOFFSET.timeOffSec, seconds);
          break;
        case 'ТН':
          var seconds = parseInt(cellTimeOff);

          var dow = cellTimeOn.substr(0, 2);
          switch (dow) {
            case 'ВС':
              dow = 0;
              break;
            case 'ПН':
              dow = 1;
              break;
            case 'ВТ':
              dow = 2;
              break;
            case 'СР':
              dow = 3;
              break;
            case 'ЧТ':
              dow = 4;
              break;
            case 'ПТ':
              dow = 5;
              break;
            case 'СБ':
              dow = 6;
              break;
          }
          var hours = parseInt(cellTimeOn.substr(3, 2));
          var minutes = parseInt(cellTimeOn.substr(6, 2));
          seconds = seconds + minutes * 60 + hours * 3600 + (dow) * 24 * 3600;
          seconds %= (24 * 3600 * 7);
          dow = parseInt(seconds / 3600 / 24);
          seconds %= (3600 * 24);
          hours = seconds / 3600;
          seconds %= 3600;
          minutes = seconds / 60;
          seconds %= 60;
          dataView.setUint8(DATAOFFSET.timeOffDow, dow);
          dataView.setUint8(DATAOFFSET.timeOffHrs, hours);
          dataView.setUint8(DATAOFFSET.timeOffMin, minutes);
          dataView.setUint8(DATAOFFSET.timeOffSec, seconds);
          break;
        case 'ЦВ':
          var seconds = parseInt(cellTimeOff);
          var minutes = parseInt(seconds / 60);
          seconds %= 60;
          dataView.setUint8(DATAOFFSET.timeOffMin, minutes);
          dataView.setUint8(DATAOFFSET.timeOffSec, seconds);

          break;
        case 'УВ':
          var hours = parseInt(cellTimeOff.substr(0, 2));
          var minutes = parseInt(cellTimeOff.substr(3, 2));
          // var inputs =
          dataView.setUint8(DATAOFFSET.timeOffHrs, hours);
          dataView.setUint8(DATAOFFSET.timeOffMin, minutes);
          // dataView.setUint8(DATAOFFSET.pwmBegin, 1);
          switch (cellInput) {}
          break;
      }
    }
    if (!(cellPwmOn == '')) {
      var pwm = parseInt(cellPwmOn);
      dataView.setUint8(DATAOFFSET.pwmBegin, pwm & 0xFF);
    }
    if (!(cellPwmOff == '')) {
      var pwm = parseInt(cellPwmOff);
      dataView.setUint8(DATAOFFSET.pwmEnd, pwm & 0xFF);
    }
    if (!(cellInput == '')) {
      switch (cellInput) {
        case 't1':
          dataView.setUint8(DATAOFFSET.inputNumber, (INPUT.T1) & 0xFF);
          break;

        case 't2':
          dataView.setUint8(DATAOFFSET.inputNumber, (INPUT.T2) & 0xFF);
          break;

        case 'Х':
          dataView.setUint8(DATAOFFSET.inputNumber, (INPUT.X) & 0xFF);
          break;
        case 'З':
          dataView.setUint8(DATAOFFSET.pwmBegin, 1);
          dataView.setUint8(DATAOFFSET.pwmEnd, 1);
          break;
        case 'Р':
          dataView.setUint8(DATAOFFSET.pwmBegin, 1);
          dataView.setUint8(DATAOFFSET.pwmEnd, 0);
          break;
        case 'РН':
          dataView.setUint8(DATAOFFSET.inputNumber, (INPUT.PH) & 0XFF);
          break;
      }
    }
    if (!(cellTresh == '')) {
      switch (cellFunction) {
        case 'ОТ':
        case 'НТ':
          var temperature = parseInt(parseFloat(cellTresh) * 16);
          dataView.setUint16(DATAOFFSET.temperature, temperature & 0xFFFF, true);
          break;
        case 'СО':
          var ph = parseInt((parseFloat(cellTresh)) * 10);
          dataView.setUint16(DATAOFFSET.temperature, ph & 0xFFFF, true);
          break;
      }
    }
  } // fill ArrayBuffer

  debugger;
  httpPostRequest(bufferTaskTable, 'stts', true);
}

function getTaskTable(dataReceived) {
  const deviceTaskStructByteCount = 15;
  // xmlHttpBinaryRequest('POST',null,messageToReceive,'stts');
  const newTableTxt = [
    [],
  ];
  const tableRowsCount = dataReceived.byteLength / deviceTaskStructByteCount;
  // console.log(tableRowsCount);
  for (let i = 0; i < tableRowsCount; i++) {
    newTableTxt[i] = [];
    const dataView = new DataView(dataReceived, i * deviceTaskStructByteCount, deviceTaskStructByteCount);
    newTableTxt[i][TASKTABLE.HANDLER] = TASK_HANDLER;
    newTableTxt[i][TASKTABLE.NUMBER] = i + 1;

    switch (dataView.getUint8(DATAOFFSET.numberLine)) {
      case NUMBERLINE.P1:
        newTableTxt[i][TASKTABLE.OUT] = 'Р1';
        newTableTxt[i][TASKTABLE.PWMON] = '';
        newTableTxt[i][TASKTABLE.PWMOFF] = '';
        break;
      case NUMBERLINE.P2:
        newTableTxt[i][TASKTABLE.OUT] = 'Р2';
        newTableTxt[i][TASKTABLE.PWMON] = '';
        newTableTxt[i][TASKTABLE.PWMOFF] = '';
        break;
      case NUMBERLINE.P3:
        newTableTxt[i][TASKTABLE.OUT] = 'Р3';
        newTableTxt[i][TASKTABLE.PWMON] = '';
        newTableTxt[i][TASKTABLE.PWMOFF] = '';
        break;
      case NUMBERLINE.P4:
        newTableTxt[i][TASKTABLE.OUT] = 'Р4';
        newTableTxt[i][TASKTABLE.PWMON] = '';
        newTableTxt[i][TASKTABLE.PWMOFF] = '';
        break;
      case NUMBERLINE.P5:
        newTableTxt[i][TASKTABLE.OUT] = 'Р5';
        newTableTxt[i][TASKTABLE.PWMON] = '';
        newTableTxt[i][TASKTABLE.PWMOFF] = '';
        break;
      case NUMBERLINE.P6:
        newTableTxt[i][TASKTABLE.OUT] = 'Р6';
        newTableTxt[i][TASKTABLE.PWMON] = '';
        newTableTxt[i][TASKTABLE.PWMOFF] = '';
        break;
      case NUMBERLINE.P7:
        newTableTxt[i][TASKTABLE.OUT] = 'Р7';
        newTableTxt[i][TASKTABLE.PWMON] = '';
        newTableTxt[i][TASKTABLE.PWMOFF] = '';
        break;
      case NUMBERLINE.P8:
        newTableTxt[i][TASKTABLE.OUT] = 'Р8';
        newTableTxt[i][TASKTABLE.PWMON] = '';
        newTableTxt[i][TASKTABLE.PWMOFF] = '';
        break;
      case NUMBERLINE.C1:
        newTableTxt[i][TASKTABLE.OUT] = 'С1';
        newTableTxt[i][TASKTABLE.PWMON] = dataView.getUint8(DATAOFFSET.pwmBegin);
        newTableTxt[i][TASKTABLE.PWMOFF] = dataView.getUint8(DATAOFFSET.pwmEnd);
        break;
      case NUMBERLINE.C2:
        newTableTxt[i][TASKTABLE.OUT] = 'С2';
        newTableTxt[i][TASKTABLE.PWMON] = dataView.getUint8(DATAOFFSET.pwmBegin);
        newTableTxt[i][TASKTABLE.PWMOFF] = dataView.getUint8(DATAOFFSET.pwmEnd);
        break;
      case NUMBERLINE.C3:
        newTableTxt[i][TASKTABLE.OUT] = 'С3';
        newTableTxt[i][TASKTABLE.PWMON] = dataView.getUint8(DATAOFFSET.pwmBegin);
        newTableTxt[i][TASKTABLE.PWMOFF] = dataView.getUint8(DATAOFFSET.pwmEnd);
        break;
      case NUMBERLINE.C4:
        newTableTxt[i][TASKTABLE.OUT] = 'С4';
        newTableTxt[i][TASKTABLE.PWMON] = dataView.getUint8(DATAOFFSET.pwmBegin);
        newTableTxt[i][TASKTABLE.PWMOFF] = dataView.getUint8(DATAOFFSET.pwmEnd);
        break;
      case NUMBERLINE.C5:
        newTableTxt[i][TASKTABLE.OUT] = 'С5';
        newTableTxt[i][TASKTABLE.PWMON] = dataView.getUint8(DATAOFFSET.pwmBegin);
        newTableTxt[i][TASKTABLE.PWMOFF] = dataView.getUint8(DATAOFFSET.pwmEnd);
        break;
      case NUMBERLINE.C6:
        newTableTxt[i][TASKTABLE.OUT] = 'С6';
        newTableTxt[i][TASKTABLE.PWMON] = dataView.getUint8(DATAOFFSET.pwmBegin);
        newTableTxt[i][TASKTABLE.PWMOFF] = dataView.getUint8(DATAOFFSET.pwmEnd);
        break;
      case NUMBERLINE.BP:
        newTableTxt[i][TASKTABLE.OUT] = 'ВР';
        newTableTxt[i][TASKTABLE.PWMON] = '';
        newTableTxt[i][TASKTABLE.PWMOFF] = '';
        break;
    }
    // var FUNCTION = _enum({TC:0,TM:1,TO:2,HT:3,OT:4,TH:5,CO:6,TK:7,UV:8});
    // var DATAOFFSET = _enum({numberLine:0,functionNumber:1,inputNumber:2,timeOnSec:3,timeOnMin:4,timeOnHrs:5,timeOnDow:6,timeOffSec:7,timeOffMin:8,timeOffHrs:9,timeOffDow:10,pwmBegin:11,pwmEnd:12,temperature:13});
    switch (dataView.getUint8(DATAOFFSET.functionNumber)) {
      case FUNCTION.TC:
        newTableTxt[i][TASKTABLE.FUNCTION] = 'ТС';
        newTableTxt[i][TASKTABLE.INPUT] = '';
        newTableTxt[i][TASKTABLE.TRESH] = '';
        newTableTxt[i][TASKTABLE.TIMEON] = `${digitToTimeView(dataView.getUint8(DATAOFFSET.timeOnHrs))}:${digitToTimeView(dataView.getUint8(DATAOFFSET.timeOnMin))}`;
        var startTime = dataView.getUint8(DATAOFFSET.timeOnHrs) * 3600 + dataView.getUint8(DATAOFFSET.timeOnMin) * 60;
        var stopTime = dataView.getUint8(DATAOFFSET.timeOffHrs) * 3600 + dataView.getUint8(DATAOFFSET.timeOffMin) * 60 + dataView.getUint8(DATAOFFSET.timeOffSec);
        var diffTime = 0;
        if (stopTime >= startTime) {
          diffTime = stopTime - startTime;
        } else {
          diffTime = stopTime + ((24 * 3600) - startTime);
        }
        newTableTxt[i][TASKTABLE.TIMEOFF] = digitToTimeView(diffTime);
        break;
      case FUNCTION.TM:
        newTableTxt[i][TASKTABLE.FUNCTION] = 'ТМ';
        newTableTxt[i][TASKTABLE.INPUT] = '';
        newTableTxt[i][TASKTABLE.TRESH] = '';
        newTableTxt[i][TASKTABLE.TIMEON] = `${digitToTimeView(dataView.getUint8(DATAOFFSET.timeOnHrs))}:${digitToTimeView(dataView.getUint8(DATAOFFSET.timeOnMin))}`;
        var startTime = dataView.getUint8(DATAOFFSET.timeOnHrs) * 3600 + dataView.getUint8(DATAOFFSET.timeOnMin) * 60;
        var stopTime = dataView.getUint8(DATAOFFSET.timeOffHrs) * 3600 + dataView.getUint8(DATAOFFSET.timeOffMin) * 60; // +dataView.getUint8(DATAOFFSET.timeOffSec);
        var diffTime = 0;
        if (stopTime >= startTime) {
          diffTime = stopTime - startTime;
        } else {
          diffTime = stopTime + ((24 * 3600) - startTime);
        }
        newTableTxt[i][TASKTABLE.TIMEOFF] = digitToTimeView(diffTime / 60);
        break;
      case FUNCTION.TO:
        newTableTxt[i][TASKTABLE.FUNCTION] = 'ТО';
        newTableTxt[i][TASKTABLE.INPUT] = '';
        newTableTxt[i][TASKTABLE.TRESH] = '';
        newTableTxt[i][TASKTABLE.TIMEON] = `${digitToTimeView(dataView.getUint8(DATAOFFSET.timeOnHrs))}:${digitToTimeView(dataView.getUint8(DATAOFFSET.timeOnMin))}`;
        newTableTxt[i][TASKTABLE.TIMEOFF] = `${digitToTimeView(dataView.getUint8(DATAOFFSET.timeOffHrs))}:${digitToTimeView(dataView.getUint8(DATAOFFSET.timeOffMin))}`;
        break;
      case FUNCTION.HT:
        newTableTxt[i][TASKTABLE.FUNCTION] = 'НТ';
        newTableTxt[i][TASKTABLE.PWMON] = '';
        newTableTxt[i][TASKTABLE.PWMOFF] = '';
        newTableTxt[i][TASKTABLE.TIMEON] = '';
        newTableTxt[i][TASKTABLE.TIMEOFF] = '';
        newTableTxt[i][TASKTABLE.INPUT] = '';
        switch (dataView.getUint8(DATAOFFSET.inputNumber)) {
          case INPUT.T1:
            newTableTxt[i][TASKTABLE.INPUT] = 't1';
            break;
          case INPUT.T2:
            newTableTxt[i][TASKTABLE.INPUT] = 't2';
            break;
          case INPUT.X:
            newTableTxt[i][TASKTABLE.INPUT] = 'Х';
            break;
        }
        newTableTxt[i][TASKTABLE.TRESH] = parseFloat(dataView.getUint16(DATAOFFSET.temperature, true)) / 16.0;
        break;
      case FUNCTION.OT:
        newTableTxt[i][TASKTABLE.FUNCTION] = 'ОТ';
        newTableTxt[i][TASKTABLE.PWMON] = '';
        newTableTxt[i][TASKTABLE.PWMOFF] = '';
        newTableTxt[i][TASKTABLE.TIMEON] = '';
        newTableTxt[i][TASKTABLE.TIMEOFF] = '';
        newTableTxt[i][TASKTABLE.INPUT] = '';
        switch (dataView.getUint8(DATAOFFSET.inputNumber)) {
          case INPUT.T1:
            newTableTxt[i][TASKTABLE.INPUT] = 't1';
            break;
          case INPUT.T2:
            newTableTxt[i][TASKTABLE.INPUT] = 't2';
            break;
          case INPUT.X:
            newTableTxt[i][TASKTABLE.INPUT] = 'Х';
            break;
        }
        newTableTxt[i][TASKTABLE.TRESH] = parseFloat(dataView.getUint16(DATAOFFSET.temperature, true)) / 16.0;
        break;
      case FUNCTION.TH:
        newTableTxt[i][TASKTABLE.FUNCTION] = 'ТН';
        newTableTxt[i][TASKTABLE.INPUT] = '';
        newTableTxt[i][TASKTABLE.TRESH] = '';
        newTableTxt[i][TASKTABLE.TIMEON] = `${dowShrt[dataView.getUint8(DATAOFFSET.timeOnDow)]} ${digitToTimeView(dataView.getUint8(DATAOFFSET.timeOnHrs))}:${digitToTimeView(dataView.getUint8(DATAOFFSET.timeOnMin))}`;
        var startTime = (dataView.getUint8(DATAOFFSET.timeOnDow) - 1) * 86400 + dataView.getUint8(DATAOFFSET.timeOnHrs) * 3600 + dataView.getUint8(DATAOFFSET.timeOnMin) * 60;
        var stopTime = (dataView.getUint8(DATAOFFSET.timeOnDow) - 1) * 86400 + dataView.getUint8(DATAOFFSET.timeOffHrs) * 3600 + dataView.getUint8(DATAOFFSET.timeOffMin) * 60 + dataView.getUint8(DATAOFFSET.timeOffSec); // +dataView.getUint8(DATAOFFSET.timeOffSec);
        var diffTime = 0;
        if (stopTime >= startTime) {
          diffTime = stopTime - startTime;
        } else {
          diffTime = stopTime + ((24 * 3600 * 7) - startTime);
        }
        newTableTxt[i][TASKTABLE.TIMEOFF] = digitToTimeView(diffTime);
        break;
      case FUNCTION.CO:
        newTableTxt[i][TASKTABLE.FUNCTION] = 'СО';
        newTableTxt[i][TASKTABLE.PWMON] = '';
        newTableTxt[i][TASKTABLE.PWMOFF] = '';
        newTableTxt[i][TASKTABLE.TIMEON] = '';
        newTableTxt[i][TASKTABLE.TIMEOFF] = '';
        newTableTxt[i][TASKTABLE.INPUT] = '';
        newTableTxt[i][TASKTABLE.TRESH] = parseFloat(dataView.getUint16(DATAOFFSET.temperature, true)) / 10.0;
        // console.log(dataView.getUint8(DATAOFFSET.temperature));
        break;
      case FUNCTION.TK:
        newTableTxt[i][TASKTABLE.FUNCTION] = 'ЦВ';
        newTableTxt[i][TASKTABLE.INPUT] = '';
        newTableTxt[i][TASKTABLE.TRESH] = '';
        newTableTxt[i][TASKTABLE.TIMEON] = digitToTimeView(dataView.getUint8(DATAOFFSET.timeOnMin) * 60 + dataView.getUint8(DATAOFFSET.timeOnSec));
        newTableTxt[i][TASKTABLE.TIMEOFF] = digitToTimeView(dataView.getUint8(DATAOFFSET.timeOffMin) * 60 + dataView.getUint8(DATAOFFSET.timeOffSec));

        break;
      case FUNCTION.UV:
        newTableTxt[i][TASKTABLE.FUNCTION] = 'УВ';
        newTableTxt[i][TASKTABLE.PWMOFF] = '';
        newTableTxt[i][TASKTABLE.PWMON] = '';
        newTableTxt[i][TASKTABLE.TIMEON] = '';
        newTableTxt[i][TASKTABLE.TIMEOFF] = `${digitToTimeView(dataView.getUint8(DATAOFFSET.timeOffHrs))}:${digitToTimeView(dataView.getUint8(DATAOFFSET.timeOffMin))}`;
        newTableTxt[i][TASKTABLE.INPUT] = '';
        newTableTxt[i][TASKTABLE.TRESH] = '';
        switch (dataView.getUint8(DATAOFFSET.pwmEnd)) {
          case 1:
            newTableTxt[i][TASKTABLE.INPUT] = 'З';
            break;
          case 0:
            newTableTxt[i][TASKTABLE.INPUT] = 'Р';
            break;
        }
        break;
    }
  }

  document.getElementById('mainTaskTable').removeChild(document.getElementById('taskTable'));
  document.getElementById('mainTaskTable').appendChild(populateTable(null, newTableTxt.length, taskTableHdr.length, newTableTxt));
  const el = document.getElementById('taskRows');
  const sortable = Sortable.create(el, {
    handle: '.drag-handle',
    onUpdate(evt/** Event */) {
      const { item } = evt; // the current dragged HTMLElement
      const table = document.getElementById('taskTable');
      const rows = table.getElementsByTagName('tr');
      const text = 'textContent' in document ? 'textContent' : 'innerText';

      for (let i = 1, len = rows.length; i < len; i++) {
        rows[i].children[1][text] = i;
      }
    },
  });
}

function taskTableBtnInit() {
  const tableEdit = document.getElementById('taskEditTable');
  const cellCount = tableEdit.rows[0].cells.length;
  // tableEdit.rows[1].cells[0].innerHTML=1;
  for (let i = 1; i < cellCount; i++) {
    tableEdit.rows[1].cells[i].childNodes[1].value = taskTableTxt[0][i];
  }
}

function xmlHttpGetBinaryRequest(messageName, callback) {
  const xmlHttpRequest = new XMLHttpRequest();
  const adr = `${adress}/${messageName}/`;

  // modal.show(LOADING);

  xmlHttpRequest.open('GET', adr, true);
  xmlHttpRequest.responseType = 'arraybuffer';
  xmlHttpRequest.onload = function () {
    const message = xmlHttpRequest.response;
    callback(message);
    // modal.hide(LOADING);
  };
  xmlHttpRequest.send();
}

function digitToTimeView(digit) {
  let timeView;
  if (digit < 0 || digit > 99) {
    return timeView;
  }
  if (digit < 10) {
    timeView = `0${digit}`;
  } else {
    timeView = digit;
  }

  return timeView;
}

function changeColor(selectItem) {
  switch (selectItem.value) {
    case '0':
      selectItem.style.color = 'red';
      break;
    case '1':
      selectItem.style.color = 'green';
      break;
    case '2':
      selectItem.style.color = 'blue';
      break;
  }
}

function editOnOff(item) {
  // item.value="";
  let string = '';
  const cldrTable = document.getElementById('calendar_tt');
  const dow = cldrTable.rows[1].cells[0].childNodes[1].childNodes[0].value;
  const hours = cldrTable.rows[1].cells[1].childNodes[1].childNodes[0].value;
  const minutes = cldrTable.rows[1].cells[2].childNodes[1].childNodes[0].value;

  const patt_minutes_seconds = /(^[0-9][0-9]$)/;
  const patt_hours = /(^([0-1][0-9]|2[0-3]):[0-5][0-9]$)/;
  const patt_dow = /^(ПН |ВТ |СР |ЧТ |ПТ |СБ |ВС )?([0-1][0-9]|2[0-3]):[0-5][0-9]$/;

  if (dow !== '') {
    string = `${dow} `;
  }
  if (hours !== '') {
    string = `${string + digitToTimeView(hours)}:`;
  }
  if (minutes !== '') {
    string += digitToTimeView(minutes);
  }

  if (patt_minutes_seconds.test(string)) {
    item.value = string;
  } else if (patt_hours.test(string)) {
    item.value = string;
  } else if (patt_dow.test(string)) {
    item.value = string;
  } else if (string == '') {
    item.value = '';
  } else {
    alert('Ошибка ввода.');
  }

  cldrTable.rows[1].cells[0].childNodes[1].value = '';
  cldrTable.rows[1].cells[1].childNodes[1].value = '';
  cldrTable.rows[1].cells[2].childNodes[1].value = '';
}

function populateSelect(cData) {
  const select = document.createElement('select');
  select.className = 'inEditTbl';

  let option = document.createElement('option');
  option.value = option.textContent = '';
  option.selected = 'true';
  select.appendChild(option);

  for (let i = 0; i <= cData; i++) {
    option = document.createElement('option');
    option.value = option.textContent = i;
    select.appendChild(option);
  }

  return select;
}

function readLedNames() {
  for (let i = 1; i <= 6; i++) {
    const key = `led${i}Name`;
    const name = localStorage.getItem(key);
    if (name) {
      $(`.${key}`).text(name);
      $(`#${key}`).val(name);
    }
  }
}

function updateLedNames() {
  for (let i = 1; i <= 6; i++) {
    const key = `led${i}Name`;
    const name = $(`#${key}`).val();
    if (name) {
      localStorage.setItem(key, name);
      $(`.${key}`).text(name);
    }
  }
}

$(document).ready(() => {
  document.getElementById('mainTaskTable').appendChild(populateTable(null, taskTableTxt.length, taskTableHdr.length, taskTableTxt));
  taskTableBtnInit();
  readLedNames();
  $('body').bootstrapMaterialDesign();

  urlString = localStorage.urlLastName;
  if (urlString !== undefined) {
    $('#inURL').val(urlString);
    adress = urlString + portString;
  }

  $('[data-toggle="tooltip"]').tooltip();

  $('#btnGetTasks').click(() => {
    xmlHttpGetBinaryRequest('gtts', getTaskTable);
  });

  $('#btnPostTasks').click(() => {
    setTaskTable();
  });

  $('#btnAddRow').click(() => {
    addRow('taskTable', rowIndexTaskTable, taskTableTxt);
  });

  $('#btnDeleteRow').click(() => {
    delRow('taskTable', rowIndexTaskTable);
  });

  $('#btnEditRow').click(() => {
    editRow('taskTable', taskTableHdr.length, taskTableTxt);
  });

  $('#btnSaveSettings').click(() => {
    localStorage.urlLastName = $('#inURL').val();
    urlString = localStorage.urlLastName;
    adress = urlString + portString;

    updateLedNames();
  });

  $('#btnGetKnobsValues').click(() => {
    httpGetRequest('gtkb', getKnobsTable);
  });

  $('#btnPostKnobsValues').click(() => {
    setKnobsTable();
  });

  $('#btnGetFanValues').click(() => {
    httpGetRequest('gtfn', getFanTable);
  });

  $('#btnPostFanValues').click(() => {
    setFanTable();
  });

  $('#btnPostDate').click(() => {
    setDate();
  });

  $('#btnStartPhCalibration').click(() => {
    calibrPhReq('cphb');
  });

  $('#btnReadyPh4').click(() => {
    calibrPhReq('cph4');
  });

  $('#btnReadyPh7').click(() => {
    calibrPhReq('cph7');
  });

  $('#btnStopPhCalibration').click(() => {
    calibrPhReq('cphe');
  });

  $('#bntGetMoonValues').click(() => {
    httpGetRequest('gtml', getMoonLightTable);
  });

  $('#bntPostMoonValues').click(() => {
    setMoonLightTable();
  });

  $('#btnStartSearchTSensor').click(() => {
    searchTSReq('tsrc');
  });

  $('#btnStopSearchTSensor').click(() => {
    searchTSReq('tstp');
  });

  $('#btnStartLedTest').click(() => {
    testLedsReq('gttb');
  });

  $('#btnEndLedTest').click(() => {
    testLedsReq('gtte');
  });

  $('#btnDispl').click(() => {
    httpGetRequest('gtdl', getDisplay);
  });

  $('#GetDisplCheck').click(() => {
    getDisplTimer();
  });

  $('select.form-control').change(function () {
    changeColor(this);
  });

  var slideout = new Slideout({
    'panel': document.getElementById('panel'),
    'menu': document.getElementById('menu'),
    'padding': 256,
    'tolerance': 70
  });

  // Toggle button
  document.querySelector('.toggle-button').addEventListener('click', function() {
    slideout.toggle();
  });
});
